# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table image (
  id                        integer not null,
  url                       varchar(255),
  constraint pk_image primary key (id))
;

create table tag (
  id                        bigint not null,
  tag                       varchar(255),
  count                     integer,
  constraint pk_tag primary key (id))
;

create table user (
  id                        bigint not null,
  username                  varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  score                     bigint,
  constraint pk_user primary key (id))
;


create table image_tag (
  image_id                       integer not null,
  tag_id                         bigint not null,
  constraint pk_image_tag primary key (image_id, tag_id))
;
create sequence image_seq;

create sequence tag_seq;

create sequence user_seq;




alter table image_tag add constraint fk_image_tag_image_01 foreign key (image_id) references image (id) on delete restrict on update restrict;

alter table image_tag add constraint fk_image_tag_tag_02 foreign key (tag_id) references tag (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists image;

drop table if exists image_tag;

drop table if exists tag;

drop table if exists user;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists image_seq;

drop sequence if exists tag_seq;

drop sequence if exists user_seq;

