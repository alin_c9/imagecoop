package controllers;

import models.game.Game;
import models.game.Player;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.startGame;
import views.html.waitForAPartner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import static controllers.LoginController.Session;


public class GameController extends Controller {
    private static final int INITIAL_NUMBER_OF_PRELOADED_IMAGES = 20;
    private final static long DELAY_TILL_REMOVE = 5; // 5 seconds

    // HashMap for the connected players
    static HashMap<String, Player> players = new HashMap<>();
    // a list for players that are waiting for a game to start
    static ArrayList<Player> holdList = new ArrayList<>();
    // the list of games
    static Vector<Game> games = new Vector<>();

    static {
        /*
         * create a new thread that will check for disconnected players
		 */
        new Thread(new Runnable() {
            private void checkForDisconnectedPlayers() {
                ArrayList<Player> listOfRemoves = new ArrayList<>();
                // loop trough all players
                for (Player player : players.values()) {
                    if (player.getLastTimeSeenInSeconds() > DELAY_TILL_REMOVE) {
                        listOfRemoves.add(player);
                    }
                }
                try {
                    // lock the collection
                    players.wait();

                    for (Player player : listOfRemoves) {
                        players.remove(player.getId());
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // unlock the collection
                players.notify();
            }

            private void checkForFinishedGames() {
                ArrayList<Game> listOfFinished = new ArrayList<>();
                for (Game game : games) {
                    if (game.isGameOutOfTime()) {
                        listOfFinished.add(game);
                    }
                }

                for (Game game : listOfFinished) {
                    games.remove(game);
                }

            }

            @Override
            public void run() {

                while (true) {

                    checkForDisconnectedPlayers();
                    checkForFinishedGames();
                    try {
                        // sleep for 5 minutes
                        Thread.sleep(1000 * 60 * 5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    // controller method for starting the game
    public static Result startGame() {
        // make sure the user has a session and is a player
        if (!Session.hasSession() || Session.getPlayer() == null) {
            return redirect(routes.LoginController.login());
        }
        // make sure the player is in a game, otherwise redirect
        // to match another player
        if (!Session.getPlayer().isInGame()) {
            return redirect(routes.GameController.matchingPlayers());
        }
        // render the image with the game
        try {
            //get player
            Player player = Session.getPlayer();
            // request game images
            String[] imagesURLs = player.getGame().getImagesURLs();
            int timeLeft = player.getGame().getTimeLeftInSeconds();
            int playerScore = player.getFinalScore();
            // check if the game is still on
            if (timeLeft <= 0) {
                player.setGame(null);
                return redirect(routes.GameController.matchingPlayers());
            }
            int roundOfTheGame = player.getGame().getRoundOfTheGame();
            return ok(startGame.render(imagesURLs, timeLeft, roundOfTheGame, playerScore));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError("We're sorry to annouce you that the server has some problems");
        }

    }

    // controller method for matching players
    public static Result matchingPlayers() {
        Game game = null;
        Player thisPlayer;
        // make sure the user has a session, otherwise redirect to login
        if (!Session.hasSession()) {
            return redirect(routes.LoginController.login());
        }
        thisPlayer = Session.getPlayer();
        // if is anybody on hold list, match it
        if (holdList.size() > 0) {
            Player thatPlayer = holdList.get(0);
            holdList.remove(0);

            // try to create a game
            try {
                game = Game.getGame();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (game == null)
                return internalServerError("We're are sorry, but due to Flickr servers our server cannot work properly, please try again in 30 seconds!"
                );
            // ad the game to the list of games
            games.add(game);
            // set up game and the players
            thisPlayer.setGame(game);
            thatPlayer.setGame(game);
            game.add(thisPlayer);
            game.add(thatPlayer);
            // redirect to the game page
            return redirect(routes.GameController.startGame());
        } else { // add player to hold list
            holdList.add(thisPlayer);
            try {
                // request some images
                String[] imagesURLs = ImagesAPIController.requestListOfImagesURL(INITIAL_NUMBER_OF_PRELOADED_IMAGES);
                return ok(waitForAPartner.render(imagesURLs));
            } catch (Exception e) {
                e.printStackTrace();
                return internalServerError("We're sorry to annouce you that the server has some problems");
            }
        }


    }

    // check if has a partner and is in game
    public static Result checkingPartner() {
        // check if the player has a session
        if (Session.hasSession()) {
            Player player = Session.getPlayer();
            if (player != null) {
                if (player.isInGame())
                    return ok("YES");
                return ok("NO");
            }
        }
        return redirect(routes.GameController.matchingPlayers());
    }

    // controller method for receiving guesses from users
    public static Result sendGuess(String guess) {
        return checkTheGuess(guess);
    }


    // check the game status
    public static Result checkGameStatus() {
        return checkTheGuess(null);
    }


    private static Result checkTheGuess(String guess) {
        if (Session.hasSession()) {
            //get the player
            Player player = Session.getPlayer();
            // check if the player is in game
            if (player.isInGame()) {
                // check if anybody left the game
                player.getGame().checkIfAnyoneLeftTheGame();
                if (guess != null) {
                    // add the guess
                    player.addGuess(guess);
                }
                // check if the guess matched with others
                return player.hasMatched() ? ok("Matched: "
                        + player.getMatchedGuess()) : ok("NOT MATCHED");
            }
        }
        return ok("NOT MATCHED");

    }
}
