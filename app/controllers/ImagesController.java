package controllers;



import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import play.mvc.Controller;
import play.mvc.Result;

import com.aetrion.flickr.Flickr;
import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.REST;
import com.aetrion.flickr.photos.Photo;
import com.aetrion.flickr.photos.PhotoList;

/**
 * Created with IntelliJ IDEA.
 * User: Alin
 * Date: 5/22/13
 * Time: 8:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class ImagesController extends Controller {
    public static final String apiKey = "355ee8cd99158cc75cf6750286adf1d9";
    public static final String sharedSecret = "d8befb2b01b41248";
    public  static Result testingAPI(){
        String rsp="";
        try {
            Flickr flickr = new Flickr(apiKey,sharedSecret,new REST());
            PhotoList photoList = flickr.getPhotosInterface().getRecent(5, 1);
            for (Object objectPhoto : photoList){
                Photo photo = (Photo) objectPhoto;
                rsp+= "<img src=\""+photo.getSmallUrl()+"\"> <br/>";
            }

        } catch (ParserConfigurationException|SAXException|FlickrException|IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return ok(rsp).as("text/html");
    }


}
