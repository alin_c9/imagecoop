package controllers;


import com.aetrion.flickr.Flickr;
import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.REST;
import com.aetrion.flickr.photos.Photo;
import com.aetrion.flickr.photos.PhotoList;
import models.images.Image;
import models.images.Tag;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ImagesAPIController extends Controller {
    private static final String flickrApiKey = "355ee8cd99158cc75cf6750286adf1d9";
    private static final String flickrSecret = "d8befb2b01b41248";


    final static String OUTPUT = "output";
    final static String IMAGE = "image";
    final static String TAGS = "tags";
    final static String UNCERTAIN_TAGS = "uncertaintags";
    final static String TAG = "tag";
    final static String ID = "id";
    final static String VALUE = "value";
    final static String SOURCE = "src";
    final static String ERROR = "error";
    final static String IN_DATABASE = "indatabase";
    final static String FALSE = "false";
    private static final String UNCERTAIN_TAG = "uncertain";
    private static final String STATUS = "status";

    public static Flickr flickr;


    // initialize flickr
    static {
        try {
            flickr = new Flickr(flickrApiKey, flickrSecret, new REST());
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
    @BodyParser.Of(BodyParser.Xml.class)
    public static Result addImages() {
        Document doc = request().body().asXml();
        ArrayList<String> imagesURLs = getValues(doc);
        if (imagesURLs != null) {
            try {
                return ok(addImagesInDatabaseAndGetStatus(imagesURLs));

            } catch (Exception e) {
                e.printStackTrace();
                internalServerError("Sorry, we're having some problems. Please try again!");
            }
        }
        return internalServerError("Please read the documentation to proper use the API.");
    }


    public static Result requestXMLOfImages(Long numberOfRequested) {
        int numberOfRequestedImages = numberOfRequested.intValue();
        try {

            if (numberOfRequestedImages > MAX_NUMBER_REQUESTED_IMAGES)
                return ok(getXMLError("Too large number")).as("text/xml");
            ArrayList<Image> listOfImages = requestListOfImages(numberOfRequestedImages);

            return ok(getXMLFromListOfImages(listOfImages)).as("text/xml");


        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            // something wrong happened
            return internalServerError("The server had a problem for now, please try again later.").as("text/xml");

        }
    }

    @BodyParser.Of(BodyParser.Xml.class)
    public static Result requestMetaInfo() {

        Document doc = request().body().asXml();
        ArrayList<String> imagesURLs = getValues(doc);
        if (imagesURLs != null) {
            try {
                String result = getMetaInfoAsStringFromListOfImages(imagesURLs);
                return ok(result).as("text/xml");
            } catch (TransformerException | ParserConfigurationException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        return badRequest("See the REST API documentation for how to use it.");

       /* try {
            String stringFromDoc = getStringFromDoc(doc);
            ArrayList<Image> listOfImages = getListOfImagesFromXMLDoc(doc);
            String result = getMetaInfoAsStringFromListOfImages(listOfImages);

            //debug
            System.out.println(stringFromDoc);

            return ok(result).as("text/xml");


        } catch (TransformerException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        return badRequest("See the REST API documentation for how to use it");*/

    }

    // retrieve a requested number of images
    public static ArrayList<Image> requestListOfImages(int numberOfRequestedImages) throws SAXException, IOException, FlickrException {
        ArrayList<Image> listOfImages = requestImagesFromDatabase(numberOfRequestedImages);
        if (listOfImages.size() < numberOfRequestedImages) {
            ArrayList<Image> listOfImagesFromInternet = requestImagesFromInternet(numberOfRequestedImages - listOfImages.size());
            listOfImages.addAll(listOfImagesFromInternet);

        }
        return listOfImages;
    }


    private static ArrayList<Image> requestImagesFromDatabase(int numberOfImages) {
       // get the images from database
        List<Image> allImages = Image.find.all();
        ArrayList<Image> listImages = new ArrayList<>();
        // loop trough all the images and add to the "listImages"
        for (Image img : allImages) {
            if (isEligible(img)) {
                listImages.add(img);
                numberOfImages--;
            }
            if (numberOfImages < 0) break;
        }
        return listImages;
    }

    // method to retrieve a list of images as String[]
    public static String[] requestListOfImagesURL(int numberOfRequestedImages) throws SAXException, IOException, FlickrException {
        ArrayList<Image> listOfImages = requestListOfImages(numberOfRequestedImages);

        String[] urls = new String[listOfImages.size()];
        int i = 0;
        for (Image img : listOfImages) {
            urls[i++] = img.getUrl();
        }

        return urls;

    }


    private static ArrayList<Image> requestImagesFromInternet(int numberOfImages) throws SAXException, IOException, FlickrException {
        PhotoList photoList;
        photoList = flickr.getPhotosInterface().getRecent(numberOfImages, 1);
        ArrayList<Image> listOfImages = new ArrayList<>();
        for (Object photoObject : photoList) {
            Photo photo = (Photo) photoObject;
            Image img = saveImageToDatabase(photo.getMediumUrl());
            listOfImages.add(img);
        }

        return listOfImages;

    }

    private final static int MAX_NUMBER_REQUESTED_IMAGES = 40;

    // check if the Image is eligible
    private static boolean isEligible(Image img) {
        if (!img.hasEnoughTags() && !img.hasEnoughCountToTags()) {
            return true;
        }

        return false;
    }

    // add photos to the database
    private static void addPhotosToDatabase(String[] imgUrls) {
        for (String imgUrl : imgUrls) {
            saveImageToDatabase(imgUrl);
        }

    }

    // save an image to the database
    private static Image saveImageToDatabase(String imgUrl) {
        if (isNotURLinDatabase(imgUrl)) {
            Image img = new Image();
            img.setUrl(imgUrl);
            img.save();
            return img;
        }
        return null;
    }

    // check the url to see if is already in the database
    private static Boolean isNotURLinDatabase(String url) {
        Image img = Image.find.where().eq("url", url).findUnique();
        // if img is null means that there is no other record with that url
        return img == null ? true : false;
    }

    // method for adding photos to the database
    private static void addPhotosToDatabase(PhotoList photoList) {
        for (Object photoObject : photoList) {
            Photo photo = (Photo) photoObject;
            saveImageToDatabase(photo.getMediumUrl());
        }

    }


    // create a XML document with a specific error
    private static String getXMLError(String error) throws ParserConfigurationException, TransformerException {
        Document doc = getXMLDocument();
        Element result = doc.createElement(OUTPUT);
        doc.appendChild(result);

        Element errorElement = doc.createElement(ERROR);
        result.appendChild(errorElement);
        errorElement.appendChild(doc.createTextNode(error));

        return getStringFromDoc(doc);
    }

    // create a XML document
    private static Document getXMLDocument() throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();

        return doc;

    }

     // create a XML from a list of images
    private static String getXMLFromListOfImages(ArrayList<Image> imgList) throws ParserConfigurationException, TransformerException {
        Document doc = getXMLDocument();
        Element result = doc.createElement(OUTPUT);
        doc.appendChild(result);
        for (Image img : imgList) {

            Element image = doc.createElement(IMAGE);
            image.setAttribute(VALUE, img.getUrl());
            result.appendChild(image);

        }
        return getStringFromDoc(doc);

    }

    // convert the DOM document to a string
    private static String getStringFromDoc(Document doc) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        transformer.transform(source, new StreamResult(writer));
        return writer.toString();
    }

    // get the meta information from a list of images as String
    private static String getMetaInfoAsStringFromListOfImages(ArrayList<String> listOfUrl) throws TransformerException, ParserConfigurationException {
        return getStringFromDoc(getXMLMetaInfoFromListOfImages(listOfUrl));
    }

// get the meta information from a list of images as DOM document
    private static Document getXMLMetaInfoFromListOfImages(ArrayList<String> listOfUrls) throws ParserConfigurationException {
        Document doc = getXMLDocument();
        Element output = doc.createElement(OUTPUT);
        doc.appendChild(output);

        for (String imageUrl : listOfUrls) {
            Element elementImage = doc.createElement(IMAGE);
            output.appendChild(elementImage);
            elementImage.setAttribute(VALUE, imageUrl);
            Image img = Image.find.where().eq("url", imageUrl).findUnique();
            if (img == null) {
                elementImage.setAttribute(IN_DATABASE, FALSE);
            } else {
                for (Tag t : img.getTags()) {
                    Element elementTag = doc.createElement(TAG);
                    elementImage.appendChild(elementTag);
                    elementTag.appendChild(doc.createTextNode(t.getTag()));
                }
            }

        }

        return doc;  //To change body of created methods use File | Settings | File Templates.
    }

    // get the a list of "VALUE"s attributes from a DOM document
    private static ArrayList<String> getValues(Document doc) {
        if (validateXML(doc)) {
            return saxParser(doc);
        }
        return null;
    }

    private final static String XSD_VALIDATOR_URL = "https://bitbucket.org/alin_c9/imagecoop/raw/423e3da14a221545b44c7291024d3719b094ecbe/other/xmlValidate.xsd";

    // method for validating a XML Document via a XSchema
    private static boolean validateXML(Document doc) {
        URL schemaFile = null;
        try {
            schemaFile = new URL(XSD_VALIDATOR_URL);
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        }
        Source xmlFile = new StreamSource(getInputStreamFromDocument(doc));
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = null;
        try {
            schema = schemaFactory.newSchema(schemaFile);
        } catch (SAXException e1) {
            e1.printStackTrace();
        }
        Validator validator = schema.newValidator();
        try {
            try {
                validator.validate(xmlFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        } catch (SAXException e) {
            return false;
        }
    }

    private static ArrayList<String> saxParser(Document doc) {
        final ArrayList<String> values = new ArrayList<>();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = null;
        try {
            saxParser = factory.newSAXParser();
        } catch (ParserConfigurationException | SAXException e1) {
            e1.printStackTrace();
        }

        DefaultHandler handler = new DefaultHandler() {

            boolean bimage = false;

            public void startElement(String uri, String localName,
                                     String qName, Attributes attributes) throws SAXException {

                if (qName.equalsIgnoreCase("IMAGE")) {
                    int len = attributes.getLength();
                    for (int i = 0; i < len; i++) {
                        String sAttrName = attributes.getLocalName(i);
                        if (sAttrName.compareTo("value") == 0) {
                            String sVal = attributes.getValue(i);
                            values.add(sVal);
                        }
                    }
                    bimage = true;
                }

            }

            public void endElement(String uri, String localName, String qName)
                    throws SAXException {
            }

            public void characters(char ch[], int start, int length)
                    throws SAXException {

                if (bimage) {
                    bimage = false;
                }

            }

        };

        try {

            saxParser.parse(getInputStreamFromDocument(doc), handler);
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }
        return values;
    }


    // get the InputStream from a XML Document
    private static InputStream getInputStreamFromDocument(Document doc) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Source xmlSource = new DOMSource(doc);
        StreamResult outputTarget = new StreamResult(outputStream);
        try {
            TransformerFactory.newInstance().newTransformer().transform(xmlSource, outputTarget);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        InputStream is = new ByteArrayInputStream(outputStream.toByteArray());
        return is;
    }

    // method for adding images to the database
    private static String addImagesInDatabaseAndGetStatus(ArrayList<String> imagesURLs) throws ParserConfigurationException, TransformerException {
        Document doc = getXMLDocument();
        Element output = doc.createElement(OUTPUT);
        doc.appendChild(output);
        for (String imageURL : imagesURLs) {
            Element imageElement = doc.createElement(IMAGE);
            output.appendChild(imageElement);
            imageElement.setAttribute(VALUE, imageURL);
            if (hasContentTypeImage(imageURL)) {


                Image img = Image.find.where().eq("url", imageURL).findUnique();
                if (img == null) {
                    img = new Image();
                    img.setUrl(imageURL);
                    img.save();
                    imageElement.setAttribute(STATUS, "SUCCESS");
                } else {
                    imageElement.setAttribute(STATUS, "Already in database");
                }
            } else {
                imageElement.setAttribute(STATUS, "Not an image");
            }
        }
        return getStringFromDoc(doc);

    }

     // check if is an image
    private static boolean hasContentTypeImage(String imageURL) {
        String contentType = getContentTypeOfURL(imageURL);
        if (contentType != null && contentType.contains("image")) {
            return true;
        } else return false;
    }

    // retrieve the content type for the imageUrl
    private static String getContentTypeOfURL(String imageURL) {
        String contentType = null;
        try {
            URL url = new URL(imageURL);
            // create a connection
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("HEAD");
            // do the actual connect
            connection.connect();
            // get the content type
            contentType = connection.getContentType();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contentType;
    }
}
