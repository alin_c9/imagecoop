package controllers;

import java.net.MalformedURLException;
import java.net.URL;

import play.mvc.Controller;
import play.mvc.Result;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import views.html.endGame;
import views.html.login;
import controllers.LoginController.Session;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.PostUpdate;

public class EndGameController extends Controller {

	/**
	 * Verifies if user has session, if it has then we store his username, score
	 * and rank and redirects user to the ranking page; if it doesn't have a
	 * session then redirects to login page
	 */
	public static Result end() {
		if (Session.hasSession()) {
			String username = session(Session.USER);
			int score = Session.getPlayer().getFinalScore();
			String rank = Session.getUser().getPlayerRank();
			return ok(endGame.render(username, score + "", rank));
		}
		return ok(login.render());
	}
	/**
	 * Verifies if user has connected with twitter, then check for a valid access token
	 * If access token is ok then we can update user's twitter status
	 */
	public static Result tweetTwitter() {
		Twitter twitter = LoginController.twitters.get(Session.ID);
		if (Session.hasTwitterSession() && twitter != null)
			try {
				int score = Session.getPlayer().getFinalScore();
				twitter.updateStatus("I have made "
						+ score
						+ " points on ImageCoop, come and join me. Play the game, change the web");
				return redirect("https://twitter.com/"
						+ twitter.getScreenName());
			} catch (TwitterException e) {
				e.printStackTrace();
			}
		return ok(login.render());
	}
	
	/**
	 * Verifies if user has connected with facebook, then check for a valid access token
	 * If access token is ok then we post to user's feed
	 */
	public static Result shareFacebook() throws FacebookException {
		Facebook fb = LoginController.fbs.get(session(Session.ID));
		PostUpdate post = null;
		if (Session.hasFacebookSession() && fb != null) {
			try {
				int score = Session.getPlayer().getFinalScore();
				post = new PostUpdate(new URL("http://imagecoop.ro"))
						.picture(
								new URL(
										"http://img196.imageshack.us/img196/4869/mediumimagecoop.png"))
						.name("ImageCoop - Guess the word, cooperate with others, change the web!")
						.caption("imagecoop.com")
						.description(
								"I have made "
										+ score
										+ " points on ImageCoop, come and join me. Play the game, change the web");
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			}
			try {
				fb.postFeed(post);

				return redirect(fb.getMe().getLink().toString());
			} catch (FacebookException e) {
				e.printStackTrace();
			}
		}
		return ok(login.render());
	}
	
	/*
	 * Checks if user has a facebook session and returns result to AJAX function
	 */
	public static Result checkFacebookAjax() {
		if (Session.hasFacebookSession())
			return ok("ok");
		return ok("not ok");
	}
	
	/*
	 * Checks if user has a twitter session and returns result to AJAX function
	 */
	public static Result checkTwitterAjax() {
		if (Session.hasTwitterSession())
			return ok("ok");
		return ok("not ok");
	}

}
