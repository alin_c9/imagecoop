package controllers;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.PictureSize;
import facebook4j.PostUpdate;
import models.game.Player;
import models.login.User;
import play.data.Form;
import play.db.ebean.Model.Finder;
import play.mvc.Controller;
import play.mvc.Result;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import views.html.login;
import views.html.ranking;
import views.html.signup;

import java.io.IOException;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginController extends Controller {

    private static final String WEBPAGE = "http://localhost:9000"; // stores adress
    private static HashMap<String, Player> players = new HashMap<>();
    // facebook
    private static final String fbApiKey = "365779333522211"; // stores ImageCoop facebook application key
    private static final String fbSecret = "f4b4121cd967bd07ec915e23a503081f"; // stores ImageCoop facebook secret key
    private static Facebook fb;
    static HashMap<String, Facebook> fbs = new HashMap<>();

    // twitter
    private static final String twitterConsumerKey = "xiMurafJ24zmtDXknrWQXw"; // stores ImageCoop twitter application consumer key
    private static final String twitterConsumerSecret = "5S05PZoNzOjrNzFttyZ7V9vAHSJ4vAvUkMqgFZ4e0"; // stores ImageCoop twitter application consumer secret
    private static Twitter twitter;
    static HashMap<String, Twitter> twitters = new HashMap<>();

    /**
     * Redirects user to twitter ImageCoop application authentication page where
     * user is asked about permissions
     *
     */
    public static Result loginTwitter() throws TwitterException, IOException {

        // prepares twitter object for getting acces token from users, sets application consumer key and consumer secret
        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.setOAuthConsumerKey(twitterConsumerKey);
        builder.setOAuthConsumerSecret(twitterConsumerSecret);
        Configuration configuration = builder.build(); // creates configuration

        TwitterFactory factory = new TwitterFactory(configuration);
        twitter = factory.getInstance(); // creates twitter object with proper configuration
        RequestToken requestToken = twitter.getOAuthRequestToken();
        return redirect(requestToken.getAuthorizationURL());

    }

    /**
     * Takes user answer from ImageCoop twitter application authorization page
     * if user accepts permissions redirects him to the game page else to the
     * login page
     */
    public static Result twitter() throws TwitterException, IOException {
        String oauth_verifier = request().getQueryString("oauth_verifier");
        if (oauth_verifier == null) { // verifies if we obtain the oauth_verifier necessary for obtaining acces token
            return ok(login.render()); // if not redirect user to login page
        } else {
            String verificat = request().getQueryString("verificat");
            if (verificat == null) {
                String uri = request().uri() + "&verificat=true";
                return redirect(WEBPAGE + uri);
            }

            AccessToken accessToken = twitter
                    .getOAuthAccessToken(oauth_verifier); // makes a request for the acces token
            twitter.setOAuthAccessToken(accessToken);
            String username = accessToken.getScreenName(); // takes users screen name from twitter
            twitter4j.User user = twitter.showUser(twitter.getId());
            String imagePath = user.getProfileImageURL(); // takes users profile image
            Session.createImageSession(imagePath); // stores profile's image url in session
            checkAccountExistenceUsername(username); // verifies if username is already in the database
            Session.createSession(username); // stores username in session
            Session.createTwitterSession(username); // creates twitter session
            Session.createIDSession();
            twitters.put(session(Session.ID), twitter); // saves twitter object
            return redirect(routes.StartPageController.startPage()); // redirect user to start page
        }
    }

    /**
     * Redirects user to facebook authentication page if user accept permissions
     * then he is logged in
     * @throws FacebookException
     */
    public static Result loginFacebook() throws MalformedURLException, FacebookException {

        String code = request().getQueryString("code"); // verifies if we obtain the code necessary for the acces token
        if (code == null) { // if code is null then we don't have it so we make another request
            Facebook facebook = new FacebookFactory().getInstance(); // creates a facebook object
            facebook.setOAuthAppId(fbApiKey, fbSecret); // sets application api key and secret
            facebook.setOAuthPermissions("email, publish_actions, publish_stream"); // sets the permissions we need for our application
            Session.createIDSession();
            fbs.put(session(Session.ID), facebook); // store facebook object
            String callbackURL = WEBPAGE + "/login/facebook";
            return redirect(facebook.getOAuthAuthorizationURL(callbackURL
                    .toString()));
        } // else we have the code

        fb = fbs.get(session(Session.ID));

        try {
            fb.getOAuthAccessToken(code); // we can make a request for acces tokens

            String email = fb.getEmail(); // get users email
            URL imageUrl = fb.getPictureURL(PictureSize.large); // get users profile image
            String imagePath = imageUrl.toString();
            Session.createImageSession(imagePath); // store users image
            checkAccountExistenceEmail(email); // verifies if users already exists in database
            Session.createSession(fb.getName()); // stores username
            Session.createFacebookSession(fb.getName()); // creates facebook session
            return redirect(routes.StartPageController.startPage()); // redirects user to start page

        } catch (FacebookException e) {
            e.printStackTrace();
            return ok(login.render()); // if something goes wrong redirect user to login page
        }
    }

    /**
     * Verifies if an email is already in database, if it isn't then it adds it
     * to database
     *
     * @param email
     *            : the email that we want to check
     */
    public static void checkAccountExistenceEmail(String email) {
        User u = new Finder<String, User>(String.class, User.class).where()
                .eq("email", email).findUnique();
        if (u == null) {
            try {
                User user = new User();
                user.setEmail(email);
                user.setUsername(fb.getName());
                user.save();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Verifies if username already exists in database, if it isn't then it adds
     * it to database
     *
     * @param username
     *            : the username that we want to check
     */
    public static void checkAccountExistenceUsername(String username) {
        User u = new Finder<String, User>(String.class, User.class).where()
                .eq("username", username).findUnique();
        if (u == null) {
            try {
                User user = new User();
                user.setUsername(username);
                user.save();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Verifies if a session is valid if it is redirect user to the main page
     * else it redirects him to the login page
     */
    public static Result login() {
        if (Session.hasSession()) {
            return redirect(routes.StartPageController.startPage());
        }
        return ok(login.render());
    }

    /**
     * Verifies if data introduced by user is correct according to data from
     * database, if it is then user is logged in and redirected to the main
     * page, else he receives error message and remains on the login page
     */
    public static Result checkLogin() {
        User user = Form.form(User.class).bindFromRequest().get();

        User u = new Finder<String, User>(String.class, User.class).where()
                .eq("email", user.getEmail())
                .eq("password", user.getPassword()).findUnique();

        if (u != null) {
            Session.createSession(user.getEmail());
            return redirect(routes.StartPageController.startPage());
        }
        return ok(login.render());
    }

    /**
     * Verifies if data introduced by user is correct according to data from
     * database, if it is then user is logged in and redirected to the main
     * page, else he receives error message and remains on the login page
     */
    public static Result ajaxCheckLogin(String email, String password) {
        User u = new Finder<String, User>(String.class, User.class).where()
                .eq("email", email).eq("password", md5(password)).findUnique();

        if (u != null) {
            return ok("true");
        }
        return ok("false");
    }

    /**
     * Redirects user to the sign up page
     */
    public static Result signUp() {
        return ok(signup.render());
    }

    /**
     * Verifies data introduced by user with data from database, if sign up is
     * correct then redirects user to the login page else remains on sign up
     * page and shows different errors
     */
    public static Result checkSignUp() {
        User user = Form.form(User.class).bindFromRequest().get();
        List<User> users = new Finder<String, User>(String.class, User.class)
                .all();
        for (User u : users) {
            if (u.getEmail().equals(user.getEmail())
                    || u.getUsername().equals(user.getUsername()))
                return ok(signup.render());
        }
        user.save();
        return redirect(controllers.routes.LoginController.login());
    }

    /**
     * Checks existence of email in database
     */
    public static Result signUpCheckEmailAjax(String email) {
        if (!checkEmail(email)) {
            return ok("wrong");
        } else {
            User u = new Finder<String, User>(String.class, User.class).where()
                    .eq("email", email).findUnique();
            if (u.getEmail().compareTo(email) == 0) {
                return ok("true");
            } else {
                return ok("false");
            }
        }
    }

    /**
     * Checks existence of username in database
     *
     * @param username
     *            : username that it must be checked
     */
    public static Result signUpCheckUsernameAjax(String username) {
        if (!checkUsername(username)) {
            return ok("wrong");
        } else {
            User u = new Finder<String, User>(String.class, User.class).where()
                    .eq("username", username).findUnique();
            if (u.getUsername().compareTo(username) == 0) {
                return ok("true");
            } else {
                return ok("false");
            }
        }
    }

    /**
     * Checks if data introduced by user respects the format of an email
     *
     * @param newEmail
     *            : email that we want to check
     */
    public static boolean checkEmail(String newEmail) {
        String theRegex = "([a-zA-Z_][a-zA-Z_0-9.]*@([a-zA-Z]+.[a-zA-Z]*)*)";
        return regexChecker(theRegex, newEmail);
    }

    /**
     * Checks if data introduced by user is an identifier
     *
     * @param newUsername
     *            : username that we want to check
     */
    public static boolean checkUsername(String newUsername) {
        String theRegex = "[a-zA-Z_][a-zA-Z_0-9]*";
        return regexChecker(theRegex, newUsername);
    }

    /**
     * Verifies if input matches regex
     *
     * @param theRegex
     *            : the pattern it must be respected
     * @param string2Check
     *            : the string that we want to verify if it respects the pattern
     */
    public static boolean regexChecker(String theRegex, String string2Check) {
        Pattern checkRegex = Pattern.compile(theRegex);
        Matcher regexMatcher = checkRegex.matcher(string2Check);
        while (regexMatcher.matches()) {
            if (regexMatcher.group().length() != 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Encrypting method for password
     */
    public static String md5(String input) {
        String md5 = null;

        if (null == input)
            return null;

        try {
            // Create MessageDigest object for MD5
            MessageDigest digest = MessageDigest.getInstance("MD5");

            // Update input string in message digest
            digest.update(input.getBytes(), 0, input.length());

            // Converts message digest value in base 16 (hex)
            md5 = new BigInteger(1, digest.digest()).toString(16);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5;
    }

    /**
     * Session class contains active sessions
     */
    public static class Session {
        final static String USER = "user";
        final static String FACEBOOK = "facebook";
        final static String TWITTER = "twitter";
        final static String SCORE = "score";
        final static String ID = "id";
        final static String IMAGE = "image";


        /**
         * verifies if we have set an user session
         *
         * @return true or false
         */
        public static boolean hasSession() {
            // session
            return (session(USER)!=null && session(PLAYER) != null) ? true : false;
        }

        /**
         * // verifies if we have set a facebook session
         *
         * @return true or false
         */
        public static boolean hasFacebookSession() {
            return session(FACEBOOK) != null ? true : false;
        }

        /**
         * verifies if we have set a twitter session
         *
         * @return true or false
         */
        public static boolean hasTwitterSession() {
            return session(TWITTER) != null ? true : false;
        }

        /**
         * creates an user session
         *
         * @param sessionString
         *            : String containing users email or username
         */
        private static final String PLAYER = "player";

        public static void createSession(String sessionString) { // creates an
            // user
            // session
            session(USER, sessionString);
            User user = new Finder<String, User>(String.class, User.class)
                    .where().eq("email", session(USER)).findUnique();
            if (user == null) {
                user = new Finder<String, User>(String.class, User.class)
                        .where().eq("username", session(USER)).findUnique();
            }
            String[] strings = request().headers().get("User-Agent");
            Player player = new Player(getAGeneratedId());
            player.setBrowser(strings[0]);
            player.setUser(user);
            session(PLAYER,player.getId());
            players.put(player.getId(), player);


        }

        /**
         * creates a score session
         *
         */
        public static void createScoreSession(String sessionString) {
            session(SCORE, sessionString);
        }

        public static void createImageSession(String sessionString) {
            session(IMAGE, sessionString);
        }

        /**
         * creates a facebook session
         *
         * @param sessionString
         *            : contains users facebook email
         */
        private static void createFacebookSession(String sessionString) {
            session(FACEBOOK, sessionString);
        }

        /**
         * creates a twitter session
         * @param sessionString: contains users twitter username
         */
        private static void createTwitterSession(String sessionString) {
            session(TWITTER, sessionString);
        }

        private static void createIDSession() {
            session(ID, getAGeneratedId());
        }

        /**
         * returns player by the information from the active session
         * @return Player
         */
        public static Player getPlayer() { // return player by the information in
            // the active session

            String playerId = session(PLAYER);
            Player player = players.get(playerId);
            if(player!=null) player.updateLastTimeSeen();
            return player;
        }

        /**
         *
         * @return user object using session information
         */
        public static User getUser() {
            User user = new Finder<String, User>(String.class, User.class)
                    .where().eq("email", session(USER)).findUnique();
            if (user == null) {
                user = new Finder<String, User>(String.class, User.class)
                        .where().eq("username", session(USER)).findUnique();
            }
            return user;
        }
    }

    /**
     *
     * @return list of users ordered by scores and ranks
     */
    public static Result clasament() {
        List<User> usersList = new Finder<String, User>(String.class, User.class)
                .orderBy("score").setMaxRows(10).findList(); // take first 10 players with biggest score
        
        ArrayList<User> users =  new ArrayList<User>();
        for(User u : usersList ) {
            users.add(0,u);
        } 
        int size = users.size();
        String[] names = new String[size];
        Long[] scores = new Long[size];
        String[] ranks = new String[size];
        int i = 0;
        for ( User u : users){
            names[i] = u.getUsername(); // store users names, scores and ranks
            scores[i] = u.getScore();
            ranks[i] = u.getPlayerRank();
            i++;
        }
        return ok(ranking.render(names, scores, ranks)); // redirect to the ranking page
    }

    private static String getAGeneratedId() {
        return java.util.UUID.randomUUID().toString();
    }

    public static Twitter getTwitter() {
        return twitter;
    }

    public static void setTwitter(Twitter twitter) {
        LoginController.twitter = twitter;
    }
}
