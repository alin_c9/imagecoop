package controllers;

import java.net.URL;

import models.login.User;
import play.mvc.Controller;
import play.mvc.Result;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import views.html.login;
import views.html.startPage;
import controllers.LoginController.Session;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.PictureSize;

public class StartPageController extends Controller {

	public static Result startPage() {
		if (Session.hasSession()) {
			User u = Session.getUser();
			String username = u.getUsername();
			Long score = u.getScore();
			String rank = u.getPlayerRank();
			if (Session.hasFacebookSession()) {
				Facebook fb = LoginController.fbs.get(session(Session.ID));
				if (fb != null) {
					String imagePath = null;
					imagePath = session(Session.IMAGE);
					return ok(startPage
							.render(username, score, rank, imagePath));
				}
			} else {
				if (Session.hasTwitterSession()) {
					String imagePath = session(Session.IMAGE);
					return ok(startPage
							.render(username, score, rank, imagePath));
				}
			}
			return ok(startPage.render(username, score, rank, null));
		}
		return ok(login.render());
	}

}
