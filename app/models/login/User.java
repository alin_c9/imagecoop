package models.login;

import models.game.RankingSystem;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@MappedSuperclass
@Entity
public class User extends Model {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    private long id;
    private String username;
    private String email;
    private String password;
    private long score;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = md5(password);
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", username = " + username + ", email=" + email + ", password=" + password
                + "]";
    }

    public static String md5(String input) {
        String md5 = null;

        if (null == input)
            return null;

        try {
            // Create MessageDigest object for MD5
            MessageDigest digest = MessageDigest.getInstance("MD5");

            // Update input string in message digest
            digest.update(input.getBytes(), 0, input.length());

            // Converts message digest value in base 16 (hex)
            md5 = new BigInteger(1, digest.digest()).toString(16);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5;
    }

    public String getPlayerRank(){
        for (RankingSystem rank : RankingSystem.values()){
            if ( score >= rank.getInferiorLimit() && score <= rank.getSuperiorLimit() ){
                return rank.toString();
            }
        }
        return "";
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score += score;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
