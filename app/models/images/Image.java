package models.images;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Image extends Model {

    @Id
    private int id; // id primary key

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Tag> getUncertainTags() {
        return uncertainTags;
    }

    public void setUncertainTags(List<Tag> uncertainTags) {
        this.uncertainTags = uncertainTags;
    }

    public static Finder<Integer, Image> getFind() {
        return find;
    }

    private String url;

    @ManyToMany
    List<Tag> tags= new ArrayList<>();
    @ManyToMany
    List<Tag> uncertainTags = new ArrayList<>();

    private final static int MINIM_NUMBER_OF_TAGS = 5;
    private final static int MINIM_COUNT_FOR_TAG = 20;

    public boolean hasEnoughCountToTags() {
        for (Tag tag : this.getTags()) {
            if (tag.getCount() < MINIM_COUNT_FOR_TAG) {
                return true;
            }
        }
        return false; // doesn't have enough count for tags
    }

    public  boolean hasEnoughTags() {
        return this.getTags().size() > MINIM_NUMBER_OF_TAGS ? true : false;
    }


    public final static Finder<Integer, Image> find
            = new Model.Finder<>(Integer.class, Image.class);


    public void addTag(String tag) {

        for(Tag t : tags){
            if(t.getTag().equals(tag)){
                t.increaseCount();
                return;
            }
        }

        Tag t = new Tag();
        t.setTag(tag);
        t.save();

        tags.add(t);

    }


    public void addUncertainTag(String uncertainTag) {

        for(Tag t : uncertainTags){
            if(t.getTag().equals(uncertainTag)){
                t.increaseCount();
                return;
            }
        }

        Tag t = new Tag();
        t.setTag(uncertainTag);
        t.save();

        uncertainTags.add(t);

    }

}
