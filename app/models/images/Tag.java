package models.images;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created with IntelliJ IDEA.
 * User: Alin
 * Date: 5/24/13
 * Time: 8:53 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity

public class Tag extends Model {
    @Id
    private long id;
    private String tag;
    private int count = 1;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getCount() {
        return count;
    }


    public void increaseCount() {
        count++;
    }

    public void decreaseCount() {
        count--;
    }
}
