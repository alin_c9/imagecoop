package models.game;

import models.login.User;

import java.util.ArrayList;

public class Player {
    private String id;
    private ArrayList<String> guesses = new ArrayList<String>();
    private Game game;
    private long lastTimeSeen = System.currentTimeMillis();
    private boolean onWaiting = false;
    private boolean onGame = false;
    private User user;
    private String browser;

    public int getFinalScore() {
        return finalScore;
    }

    public void setFinalScore(int finalScore) {
        this.finalScore = finalScore;
    }

    private int finalScore = 0;




    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        if (browser.indexOf("Firefox") != -1) {
            this.browser = "firefox";
        } else this.browser = "chrome";
    }


   // return the matched guess
    public String getMatchedGuess() {
        String retMatchedGuess = matchedGuess;
        matchedGuess = null;
        // clear the old ones
        guesses.clear();
        return retMatchedGuess;
    }

    private String matchedGuess = null;

    public Player(String id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public boolean containsGuess(String guess) {
        for (String g : guesses) {
            if (g.equals(guess)) return true;
        }
        return false;
    }


    public String getId() {
        return id;
    }

    public void addGuess(String guess) {
        if (game.isGameOutOfTime()) {
            game = null;
            onGame = false;
            System.out.println("stop game!!");
            guesses.clear();
            return;
        }
        if (guesses.contains(guess)) return;
        if (game != null) {
            // if has matched
            if (game.hasMatched(guess)) {
                matched(guess);   // mark it
            } else { // otherwise add to the list of guesses
                guesses.add(guess);
            }
        }
    }

    public void matched(String guess) {
        matchedGuess = guess;
        user.setScore(10);
        user.save();
        guesses.clear();
        finalScore+=10;
    }


    public boolean hasMatched() {
        return matchedGuess != null ? true : false;
    }


    public boolean isInGame() {
        return game != null ? true : false;
    }

    public void updateLastTimeSeen() {
        lastTimeSeen = System.currentTimeMillis();

    }
     // calculate the last time when the user was seen and
    // return the seconds
    public long getLastTimeSeenInSeconds() {
        long now = System.currentTimeMillis();
        return ((now - lastTimeSeen) / 1000);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}

