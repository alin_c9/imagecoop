package models.game;

public enum RankingSystem {
    Beginner(0, 1000),
    Amateur(1001, 2000),
    Novice(2001, 4000),
    Expert(4001, 8000),
    Master(8001, 16000),
    GrandMaster(16001, 32000);

    private int inferiorLimit, superiorLimit;

    RankingSystem(int inferiorLimit, int superiorLimit){
        this.inferiorLimit = inferiorLimit;
        this.superiorLimit = superiorLimit;
    }

    public int getInferiorLimit(){
        return inferiorLimit;
    }

    public int getSuperiorLimit(){
        return superiorLimit;
    }
}
