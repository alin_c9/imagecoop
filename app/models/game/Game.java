package models.game;

import com.aetrion.flickr.FlickrException;
import controllers.ImagesAPIController;
import models.images.Image;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;

public class Game {
    private static final int INITIAL_NUMBER_OF_PRELOADED_IMAGES = 40;
    private final static int GAME_LENGTH = 2 * 60; // 2 MINUTES
    private final static int TIME_TO_RESPOND = 10; // seconds
    private long startTime = System.currentTimeMillis();
    private ArrayList<Player> players = new ArrayList<Player>();

    public int getRoundOfTheGame() {
        return roundOfTheGame;
    }

    private int roundOfTheGame = 0;
    private ArrayList<Image> listOfImages;
    private boolean isRobotGame = false;

    private Game() {
    /* private constructor */
    }

    // check if the game s still available
    public boolean isGameOutOfTime() {
        long now = System.currentTimeMillis();
        long gameLength = ((now - startTime) / 1000); // find the seconds elapsed since the game started
        return gameLength >= GAME_LENGTH ? true : false;
    }

    // method for constructing "Game" objects
    public static Game getGame() throws SAXException, IOException, FlickrException {
        Game game = new Game();
        // request a list of images
        game.listOfImages = ImagesAPIController.requestListOfImages(INITIAL_NUMBER_OF_PRELOADED_IMAGES);
        return game;
    }


    // calculate the time left of the game
    public int getTimeLeftInSeconds() {
        long now = System.currentTimeMillis();
        long diff = (now - startTime)/1000;
        int timeLeft = (int) (GAME_LENGTH - diff);
        if(timeLeft <= 0) return 0;
        else {
            return timeLeft;
        }


    }

    public void add(Player player) {
        players.add(player);
    }

    public void remove(Player player) {
        players.remove(player);
    }

    public boolean hasMatched(String guess) {
        checkIfAnyoneLeftTheGame();
        // if he is the only left in the game, then choose random if is ok or not
        if (isRobotGame) {
            if (getImageOfTheRound().getUncertainTags().contains(guess)) {
                nextRound(guess, true);
            }

            if (Math.random() > 0.55) { // 45% to get it right
                nextRound(guess, false);
                return true;
            } else {
                return false;
            }
        }


        boolean matched = false;
        for (Player player : players) {
            if (player.containsGuess(guess)) {
                // match was done
                // mark player
                player.matched(guess);
                nextRound(guess, true);
                matched = true;
            }
        }
        return matched;
    }

    // move to the next round
    private void nextRound(String guess, boolean certain) {
        try {
            // save the tag for the image in the database
            final Image image = listOfImages.get(roundOfTheGame);
            // check if the tag was matched by both players
            if (certain == true) {
                image.addTag(guess);
            } else {
                image.addUncertainTag(guess);
            }
            image.save();
        } catch (Exception e) {
            e.printStackTrace();
        }
        roundOfTheGame++;
    }

    public Image getImageOfTheRound() {
        return listOfImages.get(roundOfTheGame);
    }


    // make sure that nobody left the game
    // otherwise set the game in the robotGame
    public void checkIfAnyoneLeftTheGame() {
        if (isRobotGame) return;
        for (Player player : players) {
            // if the player exceeded the time to respond
            if (player.getLastTimeSeenInSeconds() > TIME_TO_RESPOND) {
                player.setGame(null);
                System.out.println(player.getBrowser() + " -> was disconnected!!");
                remove(player);
                isRobotGame = true;
                return;
            }
        }
    }

    // retrieve the list of images as an array of Strings
    public String[] getImagesURLs() {
        String[] urls = new String[listOfImages.size()];
        int i = 0;
        for (Image img : listOfImages) {
            urls[i++] = img.getUrl();
        }
        return urls;
    }
}

