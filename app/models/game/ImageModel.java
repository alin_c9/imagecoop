package models.game;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Alin
 * Date: 5/24/13
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
public class ImageModel extends Model {
    @Id
    private String id;
    private String url;

    @ElementCollection(fetch = FetchType.LAZY)
    @Column(name="tags")
    private Map<String,String> tags = new HashMap<String,String>();
 //   private HashMap<String,Integer> uncertainTags;

    public Map<String,String> getTags() {
        return tags;
    }

    public void setTags(Map <String,String> tags) {
			this.tags = tags;
    }
    public void add(String key, String value){
        tags.put(key,value);
        this.save();
    }

/*    public String get(int index){
         return tags.get(index);
    }*/
/*    public String getIdPK() {
        return idPK;
    }

    public void setIdPK(String idPK) {
        this.idPK = idPK;
    }*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }






  /*  public HashMap<String, Integer> getUncertainTags() {
        return uncertainTags;
    }

    public void setUncertainTags(HashMap<String, Integer> uncertainTags) {
        this.uncertainTags = uncertainTags;
    }*/
}
