
// constant values
var ENTER_KEY = 13;
var playerGuesses = new Array();


// for the timer
var timer;
var timerTime;

$(document).ready(init);
// function for initialization
function init() {
    initTimer();
    // start the first round
    nextRound();
    $("#playerInput").keypress(function (event) {

        if (event.keyCode == ENTER_KEY) {
            addPlayerGuess($(this).val());
        }
    });

    setInterval(checkGameStatus, 500);
}
function initTimer() {
    timer = $("#timer");
    var idInterval = setInterval(updateTimer, 1000); // every second
    function updateTimer() {
        timerTime--;
        if (timerTime <= 0) {
            clearInterval(idInterval);
            window.location = "../end/game";
        }
        var minutes = Math.floor(timerTime / 60);
        var seconds = timerTime % 60;
        // to always show 2 digits
        if (seconds < 10) seconds = "0" + seconds;
        timer.html(minutes + ":" + seconds);
    }
}
function sendPlayerGuess(guess) {
    // prepare the url
    var sendUrl = "./hasMatched/" + guess;
    $.ajax(sendUrl)
        .done(function (result) {
            if (result !== "NOT MATCHED") {
                matched(result);
            }
        });

}
// the next round
function nextRound() {
    roundNo++;
    playerGuesses = new Array();
    $("#guesses").html("");
    var sourceImage = srcImages[roundNo];
    swapImage(sourceImage);

}

function notify(msg) {
    var wrapper = $("#notifierWraper");
    var notifier = $("#notifierContent");
    notifier.html(msg);
    wrapper.fadeIn("fast");
    wrapper.fadeOut(1200);

}
// a match was found
function matched(guess) {
    notify(guess);
    increaseScore();
    nextRound();

}
var reached = false;
var firstTimeExecuted = true;
// increase the score
function increaseScore() {
   // get the progress bar
    var width = $("#scoreBar").width();
    /* calculate the percentage between progress bar and the document width */
    var parentWidth = $("#scoreBar").offsetParent().width();
    var parentWidth = $("#scoreBar").offsetParent().width();
    var score = 100 * width / parentWidth;
    score = parseInt(score);
	
	// if the user alreday reached 100 (the goal)
	// set the flag "reached" as true
	if(firstTimeExecuted == true && score >= 100){
		firstTimeExecuted = false;
		reached = true;
	}
    score += 10;
    // if the player have 100 points, congratulate him
    if (score >= 100 && reached == false) {
        notify("You reached the goal!")
        reached = true;
    }
    $("#scoreBar").css("width", score + "%");
}
// function for checking with the server if has matched
function checkGameStatus() {
    var checkUrl = "./checkGameStatus";
    $.ajax(checkUrl)
        .done(function (result) {
            if (result !== "NOT MATCHED") {
                matched(result);
            }
        });

}


function addPlayerGuess(guess) {
    // if user typed a new guess
    // is not in the array, so it returns -1
    if ($.inArray(guess, playerGuesses) == -1) {
        // add to the array
        playerGuesses.push(guess);
        $("#guesses").append("<div class=\"playerGuess\">" + guess + "</div>");
        $("#playerInput").val("");
        sendPlayerGuess(guess);

    }

    // is already in array, so it's nothing to do
}

function swapImage(sourceImage) {
    $("#content").css("background", "url(" + sourceImage + ") no-repeat");

}