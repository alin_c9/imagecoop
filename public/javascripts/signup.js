$(document).ready(init);

function init() {
	$("#email").blur(function() {
		var email = $("#email").val();
		if (email != '') {
			$.ajax({
				url : "./signUpCheckEmailAjax" + "/" + email,
				success : function(result) { // verifies if email is available
					if (result == "wrong") {
						$("#email").css("color", "red");
						$("#email_warning").text("Wrong email!");
						$("#email_warning").css("visibility", "visible");
					} else {
						if (result == "true") {
							$("#email").css("color", "red");
							$("#email_warning").text("Email already exists!");
							$("#email_warning").css("visibility", "visible");
						}
					}
				}
			});
		}
	});

	$("#username").blur(
			function() {
				var username = $("#username").val();
				if (username != '') {
					$.ajax({
						url : "./signUpCheckUsernameAjax" + "/" + username,
						success : function(result) { // verifies if username is available
							if (result == "wrong") {
								$("#username").css("color", "red");
								$("#username_warning").text("Wrong username!");
								$("#username_warning").css("visibility", "visible");
							} else {
								if (result == "true") {
									$("#username_warning").text(
											"Username already exists!");
									$("#username_warning").css("visibility",
											"visible");
									$("#username").css("color", "red");
								}
							}
						}
					});
				}
			});

	$("#password").blur(function() {
		var password = $("#password").val();
		var repassword = $("#repassword").val();
		if (repassword != '') {
			if (password != repassword) { // verifies if passwords match
				$("#password_warning").text("Passwords don't match!")
				$("#password_warning").css("visibility", "visible");
			}
		}
	});

	$("#repassword").blur(function() { // verifies if passwords match
		var password = $("#password").val();
		var repassword = $("#repassword").val();
		if (password != repassword) {
			$("#password_warning").text("Passwords don't match!")
			$("#password_warning").css("visibility", "visible");
		}
	});

	$("#username").focus(function() {
		$("#username_warning").css("visibility", "hidden");
		$("#fields_warning").css("visibility", "hidden");
		$("#username").css("color", "black");
	});

	$("#email").focus(function() {
		$("#email_warning").css("visibility", "hidden");
		$("#fields_warning").css("visibility", "hidden");
		$("#email").css("color", "black");
	});

	$("#password").focus(function() {
		$("#password_warning").css("visibility", "hidden");
		$("#fields_warning").css("visibility", "hidden");
		$("#password").val("");
	});

	$("#repassword").focus(function() {
		$("#password_warning").css("visibility", "hidden");
		$("#fields_warning").css("visibility", "hidden");
		$("#repassword").val("");
	});

	$("#signupbutton").click(function() {
		var pass = $("#password").val();
		var cPass = $("#repassword").val();
		var email = $("#email").val();
		var username = $("#username").val();
		if (email == '' || username == '' || cPass == '' || pass == '') { // verifies if user completed all fields
			$("#fields_warning").css("visibility", "visible");
			return false;
		}
	});
}