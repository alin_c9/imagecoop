$(document).ready(init)
function init() {

	$.ajax({
		url : './checkTwitterAjax',
		success : function(result) { // verifies if user has a twitter session
			if (result == "ok") { // if it has then he can make a tweet
				$("#title3").css("visibility", "visible");
				$("#twitterButton").css("visibility", "visible"); // make tweet button visible, tweet button is hidden by default
			}
		}
	});
	
	$.ajax({
		url : './checkFacebookAjax',
		success : function(result) { // verifies if user has a facebook session
			if (result == "ok") { // if it has then he can make a facebook post
				$("#title3").css("visibility", "visible");
				$("#facebookButton").css("visibility", "visible"); // make share button visible
			}
		}
	});
}