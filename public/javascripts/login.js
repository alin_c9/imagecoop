$(document).ready(init);

function init() {
	
	$("#email").blur(function() {
		var email = $("#email").val();
		var password = $("#password").val();
		if (email != '' && password != '') {
			$.ajax({
				url : "./ajaxCheckLogin" + "/" + email + "/" + password,
				success : function(result) { // verifies if email exists in database
					if (result == "false") { // if it doesn't exist then user inserted a wrong email
						$("#email").css("color", "red"); //
						$("#password").css("color", "red");
						$("#warning").text("Wrong email or password!");
						$("#warning").css("visibility", "visible");
					}
				}
			});
		}
	});
	
	$("#password").blur(function() {
		var email = $("#email").val();
		var password = $("#password").val();
		if (email != '' && password != '') {
			$.ajax({
				url : "./ajaxCheckLogin" + "/" + email + "/" + password,
				success : function(result) {
					if (result == "false") { // checks if user inserted a correct password
						$("#email").css("color", "red");
						$("#password").val('');
						$("#warning").text("Wrong email or password!");
						$("#warning").css("visibility", "visible");
					}
				}
			});
		}
	});
	
	$("#password").focus(function() {
		$("#warning").css("visibility", "hidden");
		$("#password").css("color", "black");
	});

	$("#email").focus(function() {
		$("#warning").css("visibility", "hidden");
		$("#email").css("color", "black");
	});
	
	$("#signinbutton").click(function() {
		var pass = $("#password").val();
		var email = $("#email").val();
		if (email == '' || pass == '') { // cheks if user completed all fields
			$("#warning").text("Wrong email or password!");
			$("#warning").css("visibility", "visible");
			return false;
		}
	});
}