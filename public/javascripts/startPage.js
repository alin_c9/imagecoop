$(document).ready(init);

function init() {
	var rank = $("#rank").text(); // verifies users rank and creates a corespondent badge
	if (rank == "Beginner")
		var badge = $('<span class="badge">1st level</span>');
	if (rank == "Amateur")
		var badge = $('<span class="badge badge-success">2nd level</span>');
	if (rank == "Novice")
		var badge = $('<<span class="badge badge-warning">3rd level</span>');
	if (rank == "Expert")
		var badge = $('<span class="badge badge-important">4th level</span>');
	if (rank == "Master")
		var badge = $('<span class="badge badge-info">5th level</span>');
	if (rank == "GrandMaster")
		var badge = $('<span class="badge badge-inverse">6th level</span>');
	var image = $("#profile_image").attr("src");
	if ( image == "") // verifies if we obtain an image, if we haven't then we hide it
		$('#profile_image').css("visibility", "hidden");
	else 
		$('#profile_image').css("visibility", "visible");

	$("#content").append(badge); // adds badge to page	
}
