/**
 * Created with IntelliJ IDEA.
 * User: Alin
 * Date: 5/10/13
 * Time: 5:40 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(init);

function init() {
    var checkingIfHasPartnerUrl = "./checkingPartner";

    setInterval(checking, 500);

    function checking() {
        $.ajax(checkingIfHasPartnerUrl)
            .done(function (result) {
                if (result == "YES") { // found a partner
                    window.location.href = "./startGame";
                }
            });
    }
}